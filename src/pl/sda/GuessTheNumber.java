package pl.sda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class GuessTheNumber {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean result = false;
        int numberOfGuesses = 1;
        Random rand = new Random();
        int number = rand.nextInt(10);

        while(result == false)
        {
            System.out.println("Wprowadź liczbę z zakresu 0-10: ");
            int guess = Integer.parseInt(reader.readLine());
            if(guess == number) {
                System.out.println("Znalazłeś szukaną liczbę!!!");
                result = true;
            }else if (guess > number) {
                System.out.println("Podałeś większą liczbę.");
                result = false;
                numberOfGuesses++;
            }else if (guess < number){
                System.out.println("Podałeś mniejszą liczbę.");
                result = false;
                numberOfGuesses++;
            }
        }
        System.out.println("Zgadłeś za " + numberOfGuesses + " razem.");
    }
}
